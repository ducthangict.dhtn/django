Document:
1. Requirement
- python 3.8.x or higher.
- pip3 install Django==3.2 or pip3 install Django==3.2
2. Run
Active env

- source venv/bin/activate
- python3 manager.py runserver

3. Link access:
http://localhost:8000 or http://127.0.0.1:8000

access addmin with:
user: admin 
pass: admin
link: http://127.0.0.1:8000/admin 
