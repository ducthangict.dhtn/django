from django.shortcuts import render
from .models import Product

# Create your views here.
def product_detail(request, pk):
    product = Product.objects.get(pk=pk)
    context = {
        'product': product
    }
    return render(request, 'product_detail.html', context)