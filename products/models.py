from django.db import models

# Create your models here.


class Product(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    price_unit = models.FloatField()
    qty = models.IntegerField()
    image = models.CharField(max_length=255)

class ProductCategory(models.Model):
	name = models.CharField(max_length=100)
	code = models.CharField(max_length=100)