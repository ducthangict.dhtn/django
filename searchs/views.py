from django.shortcuts import render
from django.db.models import Q
from projects.models import Project
from products.models import Product

# Create your views here.


def search_item_index(request):
    posts = []
    search_post = request.GET.get('search')
    if search_post:
    	posts = Project.objects.filter(Q(title__icontains=search_post) | Q(description__icontains=search_post))
    	if not posts:
    		posts = Product.objects.filter(Q(title__icontains=search_post) | Q(description__icontains=search_post))

    context = { 'blogs': posts }

    return render(request, 'search_index.html', context)