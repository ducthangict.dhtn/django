from django.urls import path
from . import views
from projects import views as bviews
urlpatterns = [
    path("", views.home_index, name="index"),
    path("abouts/", views.home_about, name="abouts"),
    path("blog/<int:pk>/", bviews.project_detail, name="blog_detail"),
]