from django.shortcuts import render
from projects.models import Project
from products.models import Product

# Create your views here.

def get_home_blogs():
	blogs = Project.objects.all()
	return blogs

def get_home_products():
	products = Product.objects.all()
	return products

def home_index(request):
    blogs = get_home_blogs()
    products = get_home_products()

    context = {
        'blogs': blogs,
        'products': products
    }
    return render(request, 'home_index.html', context)

def home_about(request):
    context = {
    }
    return render(request, 'abouts.html', context)